## Open 54 Realization Dataset
This is a dataset that can be used in the NRAP-OpenIAM
https://gitlab.com/NRAP/OpenIAM

This simulation data taken from:
Wainwright, H.; Finsterle, S.; Zhou, Q.; Birkholzer, J. 
Modeling the Performance of Large-Scale CO2 Storage Systems: 
A Comparison of Different Sensitivity Analysis Methods 
NRAP-TRS-III-002-2012 
NRAP Technical Report Series
U.S. Department of Energy, National Energy Technology Laboratory: Morgantown, WV,
2012; p 24.

## Closed 200 Realization Dataset
Data based on Kimberlina compartmentalized reservoir model obtained from LBNL in 2013, Source:

J.T. Birkholzer, Q. Zhou, A. Cortis, S. Finsterle,
A Sensitivity Study on Regional Pressure Buildup from Large-Scale CO2 Storage Projects,
Energy Procedia,
Volume 4,
2011,
Pages 4371-4378,
ISSN 1876-6102,
https://doi.org/10.1016/j.egypro.2011.02.389.
(http://www.sciencedirect.com/science/article/pii/S1876610211006680)
Keywords: Brine pressurization; Basins; Faults; Compartments

Reservoir injection occurs at:

x,y=300025.19, 3934175.82
node 585

